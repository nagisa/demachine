; Programa randa pavienių (nesudvigubintų) ‘E’, ‘F’ arba ‘G’ raidžių skaičius
; kiekvienoje eilutėje ir išveda juos 16-tainiu pavydalu į kitą failą.

.model small
.stack 100h

BUF_SIZE    EQU 100h

.data

;; Internal data

tmpByte     db 0
tmpWord     dw 0
tmpHex      db 3 dup ('$')

;; Utilities
nl          db 13, 10, '$'

;; File opening
in_fname    db BUF_SIZE dup(0)
in_desc     dw -1
query_in    db "Enter filename of the input file:", 13, 10, "$"
error_in    db "Could not open the input file.", 13, 10, "$"
current_idx dw 0100h

;; Reading
buffer      db BUF_SIZE dup(0), 0
error_read  db "Could not read a byte.", 13, 10, "$"

;; Instructions
instr_err   db "Unknown instruction found.", 13, 10, "$"
bug         db "BUG: matched instruction didn’t jump back", 13, 10, "$"
letterh     db "h$"

movI         db "mov $"
pushI        db "push $"
popI         db "pop $"
callI        db "call $"
retI         db "ret $"
retfI        db "retf $"
notI         db "not $"
negI         db "neg $"
mulI         db "mul $"
imulI        db "imul $"
divI         db "div $"
idivI        db "idiv $"
joI          db "jo $"

wPtr        db "word ptr $"
bPtr        db "byte ptr $"

wRegisters  db "AX$CX$DX$BX$SP$BP$SI$DI$"
bRegisters  db "AL$CL$DL$BL$AH$CH$DH$BH$"
sRegisters  db "ES$CS$SS$DS$"

r2bxsi      db "BX+SI$"
r2bxdi      db "BX+DI$"
r2bpsi      db "BP+SI$"
r2bpdi      db "BP+DI$"
r1si        db "SI$"
r1di        db "DI$"
r1bp        db "BP$"
r1bx        db "BX$"
plus        db "+$"

.code

LOCALS @@

;; --------
;; MACROSES
;; --------

printFromReg MACRO reg
    push    dx
    push    ax
    mov     dx, reg
    mov     ah, 09h
    int     21h
    pop     ax
    pop     dx
ENDM

print MACRO data
    push    dx
    mov     dx, offset data
    printFromReg dx
    pop     dx
ENDM

printChar MACRO char
push ax
push dx
mov ah, 02h
mov dl, char
int 21h
pop dx
pop ax
ENDM

openRead MACRO fname,desc,onfail
LOCAL @@opened
    clc
    push    dx
    push    ax
    mov     ah, 3Dh
    mov     al, 00
    mov     dx, offset fname
    int     21h
    mov     [desc], ax
    pop     ax
    pop     dx
    jnc     @@opened
    jmp     onfail
    @@opened:
ENDM

openWrite MACRO fname,desc,onfail
LOCAL @@opened
    clc
    push    dx
    push    ax
    push    cx
    mov     ah, 3Ch
    mov     cx, 0
    mov     al, 0
    mov     dx, offset fname
    int     21h
    mov     [desc], ax
    pop     cx
    pop     ax
    pop     dx
    jnc     @@opened
    jmp     onfail
    @@opened:
ENDM

closeFile MACRO desc
LOCAL @@closed
    cmp     desc, -1
    je      @@closed
    mov     bx, desc
    mov     ah, 3Eh
    int     21h
    mov     desc, -1
@@closed:
ENDM

readByte MACRO desc,target,onfail
LOCAL @@ok
    push cx
    push ax
    push dx
    push bx
    mov bx, desc
    mov cx, 1
    mov ah, 3Fh
    mov dx, offset tmpByte
    int 21h
    cmp ax, 0
    pop bx
    pop dx
    pop ax
    pop cx
    jne @@ok
    jmp onfail
    @@ok:
    add [current_idx], 1
    mov target, [tmpByte]
ENDM

getInput MACRO maxLen,buf
    push    ax
    push    dx
    mov     ah, 0Ah
    mov     byte ptr[buf], maxLen
    mov     dx, offset buf
    int     21h
    push    bx
    mov     bx, word ptr [buf + 1]
    mov     bh, 0
    mov     byte ptr [buf + bx + 2], 0
    pop     bx
    pop     dx
    pop     ax
ENDM

matchInstr MACRO instr,mask,eql,target
LOCAL @@notInstr
     push ax
     mov al, instr
     and al, mask
     cmp al, eql
     pop ax
     jne @@notInstr
     jmp target
     @@notInstr:
ENDM

printHexWord MACRO register
push ax
mov ax, register
xchg al, ah
call printHexByte
xchg al, ah
call printHexByte
printChar 'h'
pop ax
ENDM

printSignedHexWord MACRO register
LOCAL @@noSign,@@done
    push ax
    push dx
    mov ax, register
    mov dx, register
    shr dx, 0Eh
    cmp dx, 0
    je @@noSign
    printChar '-'
    neg ax
    and ax, 7FFFh
    xchg ah, al
    call printHexByte
    xchg ah, al
    call printHexByte
    jmp @@done
    @@noSign:
    xchg ah, al
    call printHexByte
    xchg ah, al
    call printHexByte
    @@done:
    printChar 'h'
    pop dx
    pop ax
ENDM

printSignedHexByte MACRO register
LOCAL @@noSign,@@done
    push ax
    mov al, register
    mov ah, register
    shr ah, 7
    cmp ah, 0
    je @@noSign
    printChar '-'
    neg al
    and al, 07Fh
    call printHexByte
    jmp @@done
    @@noSign:
    call printHexByte
    @@done:
    pop ax
ENDM

;; ----------
;; PROCEDURES
;; ----------

quit proc
    closeFile [in_desc]
    mov       ah, 4Ch
    int       21h
quit endp

;; Will print unsigned AL value as hex.
printHexByte proc
    push    ax
    push    dx
    ; Ensure ah is empty.
    mov     ah, 0
    mov     dl, 10h
    div     dl
    mov     dl, al
    mov     dh, ah
    ; First nibble
    cmp     dl, 10
    jl      @@hexFalse1
    add     dl, 7
    @@hexFalse1:
    add     dl, '0'
    mov     [tmpHex], dl
    ; Second nibble
    cmp     dh, 10
    jl      @@hexFalse2
    add     dh, 7
    @@hexFalse2:
    add     dh, '0'
    mov     [tmpHex + 1], dh

    print   tmpHex
    pop     dx
    pop     ax
    ret
printHexByte endp

; Prints register with code from AL and wideness from AH (if != 0, then wide)
printReg proc
    push ax
    push dx
    cmp ah, 0
    je @@notWide
    mov dx, offset wRegisters
    jmp @@wideEnd
    @@notWide:
    mov dx, offset bRegisters
    @@wideEnd:
    mov ah, 3
    mul ah
    add dx, ax
    printFromReg dx
    pop dx
    pop ax
    ret
printReg endp

; Prints segment register with code from AL
printSegReg proc
    push ax
    push dx
    mov ah, 3
    mul ah
    mov dx, offset sRegisters
    add dx, ax
    printFromReg dx
    pop dx
    pop ax
    ret
printSegReg endp

; Decodes modregrm byte in AL into
; ah: mod
; bl: reg
; bh: r/m
; NOTE: depending on ah, additional bytes may need to be read and decoded
decodeModRegRm proc
    mov ah, al
    mov bl, al
    mov bh, al
    shr ah, 6
    shr bl, 3
    and bl, 7
    and bh, 7
    ret
decodeModRegRm endp

; Prints effective address register combination (i.e. BX + SI) for a given code
; in AL
printModEA proc
    cmp al, 0
    jne @@not0
    print r2bxsi
    ret
    @@not0:
    cmp al, 1
    jne @@not1
    print r2bxdi
    ret
    @@not1:
    cmp al, 2
    jne @@not2
    print r2bpsi
    ret
    @@not2:
    cmp al, 3
    jne @@not3
    print r2bpdi
    ret
    @@not3:
    cmp al, 4
    jne @@not4
    print r1si
    ret
    @@not4:
    cmp al, 5
    jne @@not5
    print r1di
    ret
    @@not5:
    cmp al, 6
    jne @@not6
    print r1bp
    ret
    @@not6:
    print r1bx
    ret
printModEA endp

; Disassembles and prints mod *** r/m
; ah: mod
; al: is wide? (used for mod = 3 case only)
; bh: r/m
disassembleModRm proc
    cmp ah, 3
    jne @@not11
    mov ah, al
    mov al, bh
    call printReg
    ret


    @@not11:

    ; Print word ptr / byte ptr (applicable for everything below)
    cmp al, 0
    jne @@wide
    print bPtr
    jmp @@endWpBp
    @@wide:
    print wPtr
    @@endWpBp:

    cmp ah, 2
    je @@mod10
    jmp @@not10
    @@mod10:
    printChar '['
    mov al, bh
    call printModEA
    print plus
    readByte in_desc,al,read_fail
    readByte in_desc,ah,read_fail
    printHexWord ax
    printChar ']'
    ret

    @@not10:
    cmp ah, 1
    je @@is01
    jmp @@not01

    @@is01:
    printChar '['
    mov al, bh
    call printModEA
    readByte in_desc,al,read_fail
    cmp al, 0
    jl @@noPlus
    print plus
    @@noPlus:
    printSignedHexByte al
    printChar 'h'
    printChar ']'
    ret

    @@not01:
    printChar '['
    cmp bh, 6
    jne @@notbh6
    readByte in_desc,al,read_fail
    readByte in_desc,ah,read_fail
    printHexWord ax
    printChar ']'
    ret
    @@notbh6:
    mov al, bh
    call printModEA
    printChar ']'
    ret
disassembleModRm endp

;; ---------
;; MAIN CODE
;; ---------

main:

;; Set up data segment
mov     ax, @data
mov     ds, ax

;; Set up in file
print query_in
getInput 11,buffer
openRead buffer+2,in_desc,in_fail

jmp dsm

in_fail:
print error_in
jmp finish

;; Disassembler
dsm:
print nl
readByte in_desc,al,finish

matchInstr al,0FEh,0F6h,nnmmddInstrs
matchInstr al,0FCh,088h,movInstr
matchInstr al,0FFh,070h,joInstr
matchInstr al,0F8h,050h,pushRegInstr
matchInstr al,0F8h,058h,popRegInstr
matchInstr al,0E7h,006h,pushSegInstr
matchInstr al,0E7h,007h,popSegInstr
matchInstr al,0FFh,0FFh,pushRmICallInstrs
matchInstr al,0FFh,08Fh,popRmInstr
matchInstr al,0FFh,0E8h,dCallWInstr
matchInstr al,0FFh,0C3h,retInstr
matchInstr al,0FFh,0CBh,retfInstr
matchInstr al,0FFh,0C2h,retspInstr
matchInstr al,0FFh,0CAh,retfspInstr

print instr_err
jmp finish

read_fail:
print error_read
jmp finish

finish:
call quit

movInstr: ; mov (1)
print movI
mov dl, al
and dl, 1 ; dl = is wide?
mov dh, al
and dh, 2 ; dh = direction
readByte in_desc,al,read_fail
call decodeModRegRm
cmp dh, 0
je movRegToRmInstr
jmp movRmToRegInstr

movRegToRmInstr:
push ax
push dx
mov al, dl
call disassembleModRm
printChar ','
pop dx
pop ax
mov al, bl
mov ah, dl
call printReg
jmp dsm

movRmToRegInstr:
push ax
push dx
mov al, bl
mov ah, dl
call printReg
printChar ','
pop dx
pop ax
mov al, dl
call disassembleModRm
jmp dsm


; Instructions that depend on subsequent bytes
;; ---

pushRmICallInstrs: ; push [register/memory], call register, call register:register
readByte in_desc,al,read_fail
call decodeModRegRm
cmp bl, 6
je pushRMInstr
jmp dsm

pushRmInstr:
print pushI
mov al, 1
call disassembleModRm
jmp dsm

;; ---

nnmmddInstrs: ; not, neg, mul, imul, div, idiv
mov dl, al
and dl, 1 ; dl = is wide?
readByte in_desc,al,read_fail
call decodeModRegRm
push ax
push bx
mov ah, dl
mov al, bh
cmp bl, 2
je notInstr
cmp bl, 3
je negInstr
cmp bl, 4
je mulInstr
cmp bl, 5
je imulInstr
cmp bl, 6
je divInstr
cmp bl, 7
je idivInstr
nnmmddInstrs2:
pop bx
pop ax
mov al, dl
call disassembleModRm
jmp dsm

notInstr:
print notI
jmp nnmmddInstrs2

negInstr:
print negI
jmp nnmmddInstrs2

mulInstr:
print mulI
jmp nnmmddInstrs2

imulInstr:
print imulI
jmp nnmmddInstrs2

divInstr:
print divI
jmp nnmmddInstrs2

idivInstr:
print idivI
jmp nnmmddInstrs2

;; ---

; Implemented instrs go below

retInstr: ; ret
print retI
jmp dsm

retfInstr: ; retf
print retfI
jmp dsm

retspInstr: ; ret [16 bits]
print retI
readByte in_desc,al,read_fail
readByte in_desc,ah,read_fail
printHexWord ax
jmp dsm

retfspInstr: ; retf [16 bits]
print retfI
readByte in_desc,al,read_fail
readByte in_desc,ah,read_fail
printHexWord ax
jmp dsm

pushRegInstr: ; push [register]
print pushI
and al, 07h
mov ah, 1
call printReg
jmp dsm

popRegInstr: ; pop [register]
print popI
and al, 07h
mov ah, 1
call printReg
jmp dsm

pushSegInstr: ; push [segment]
print pushI
shr al, 3
and al, 07h
call printSegReg
jmp dsm

popSegInstr: ; pop [segment]
print popI
shr al, 3
and al, 07h
call printSegReg
jmp dsm

popRmInstr: ; pop [register/memory]
print popI
readByte in_desc,al,read_fail
call decodeModRegRm
mov al, 1
call disassembleModRm
jmp dsm

joInstr: ; jo
print joI
readByte in_desc,al,read_fail
mov cx, [current_idx]
cmp al, 0
jl joInstrNeg
mov ah, 0
add cx, ax
printHexWord cx
jmp dsm

joInstrNeg:
mov ah, 0
neg al
sub cx, ax
printHexWord cx
jmp dsm

dCallWInstr: ; call label
print callI
readByte in_desc,al,read_fail
readByte in_desc,ah,read_fail
mov cx, [current_idx]
cmp ax, 0
jl dCallWInstrNeg
add cx, ax
printHexWord cx
jmp dsm

dCallWInstrNeg:
neg ax
sub cx, ax
printHexWord cx
jmp dsm



print bug
call quit
end main
